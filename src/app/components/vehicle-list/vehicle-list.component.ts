import { Component, OnInit } from '@angular/core';
import { VehicleApi, LoopBackConfig } from './../../sdk';
import { ToastrService } from 'ngx-toastr';


declare const $: any;

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})

export class VehicleListComponent implements OnInit {
  status:any = "approved";	
  skip: any = 0;
  limit: any = 10;
  searchStr:any = "";
  vehicles:any = [];
  isLoading:any = false;
  options:any = {
    statusList:["Pending","Rejected","Approved"],
    selectedStatus:{
      text:"Pending",
      id:"Pending"
    },
  }
  baseUrl:string = "";
  approvalValue : string;
  selectedVehicle  : any = {};

  constructor(private toastr: ToastrService, private vehicleApi:VehicleApi) { }

  ngOnInit() {
    this.baseUrl = LoopBackConfig.getPath();
  	this.getAllVehicle();
  }

  getAllVehicle(){
    this.getStatus();
    this.isLoading = true;

  	this.vehicleApi.getAllVehicle(this.status, this.skip, this.limit, this.searchStr).subscribe((vehicles)=>{
  		this.vehicles = vehicles.success.data;	
  		// console.log(this.vehicles);
      this.isLoading = false;
  	},(error)=>{
      this.isLoading = false;
      this.toastr.error(error.message);
  		// console.log(error);
  	})
  }

  getStatus(){
    console.log(this.options);
    if(this.options.selectedStatus.text == "Approved"){
      this.status = 'approved';
    }else if(this.options.selectedStatus.text == "Pending"){
      this.status = 'pending';
    }else if(this.options.selectedStatus.text == "Rejected"){
      this.status = 'rejected';
    }
    /*if(this.options.searchStr && this.options.searchStr !=""){
      this.filter.where.fullName = {like:this.options.searchStr,options:"i"};
    }*/
  }

  selected(data?:any){
    // this.options.selectedStatus = data;
    // console.log("selected data => ",data);
    this.getAllVehicle();
  }

  openApprovalModal(value,vehicle){
    this.approvalValue = value;
    this.selectedVehicle = vehicle;
    $("#approveModal").modal("show");
  }

  adminApproval(appForm?:any){
    let $btn = $('#approvalBtn');
    $btn.button('loading');
    this.vehicleApi.approve(this.selectedVehicle.id,this.approvalValue).subscribe((success)=>{
      $btn.button('reset');
      this.toastr.success("Successfully "+this.approvalValue);
      $("#approveModal").modal("hide");
      this.getAllVehicle();
    },(error)=>{
      $btn.button('reset');
      this.toastr.error(error.message);
    })
  }

  openModal(vehicle){
    this.selectedVehicle = vehicle;
    console.log(this.selectedVehicle);
    $("#formModal").modal("show");
  }


}
