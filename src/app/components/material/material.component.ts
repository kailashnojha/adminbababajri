import { Component, OnInit, ViewChild } from '@angular/core';
import { MaterialApi , LoopBackAuth} from './../../sdk/index';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import * as _ from 'lodash';
import { DefaultDataService } from './../../services/default-data/default-data.service';

declare const $: any;
@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {
  // @ViewChild("materialForm") materialForm:NgForm;
  selectedMaterial : any = {
    name:{
      en:"",
      hi:""
    },
    units:[]
  };
  materials : Array<any> = [];
  errSuccMaterial : any = {
    isError : false,
    isSuccess : false,
    succMsg : "",
    errMsg : ""
  }
  defaultUnits:any = [];
  isSubTypesUnique:boolean= true;
  
  @ViewChild("materialForm") matForm;

  constructor(private defaultData:DefaultDataService, private toastr: ToastrService, private materialApi:MaterialApi) { 
    this.defaultUnits = this.defaultData.getUnits();
    
  }

  ngOnInit() {
  	this.getMaterials();
  	this.selectedMaterial.subTypes = [];
    // console.log(this.materialForm)
  }

  getMaterials(){
  	this.materialApi.getMaterials().subscribe((success)=>{
  		this.materials = success.success.data;
      
  	},(error)=>{
      this.toastr.error(error.message);
  	})
  }

  openModal(material?:any){

      if(material){
        this.selectedMaterial = JSON.parse(JSON.stringify(material)); //Object.assign({}, material);
        if( typeof this.selectedMaterial.name == 'string' ){
          this.selectedMaterial.name =  {
            en:"",
            hi:""
          }
        }

      }else{
        this.matForm.resetForm();
        this.selectedMaterial = {
          name:{
            en:"",
            hi:""
          },
          units:[],
          subTypes:[]
        };

        // this.matForm.resetForm();
      }  
      // console.log(this.material)
      this.selectedMaterial.subTypes = this.selectedMaterial.subTypes || [];
      this.errSuccMaterial = {
        isError:false,
        isSuccess:false,
        succMsg:"",
        errMsg:""
      }

      // console.log(this.selectedMaterial);

      $("#materialModal").modal("show")
  }	

  openDelModal(material){
    this.selectedMaterial = Object.assign({}, material);
    $("#alertDelMat").modal("show");
  }

  delMatData(){
    let $btn = $('#delMatBtn');
    $btn.button('loading');
    this.materialApi.delMaterial(this.selectedMaterial.id).subscribe((success)=>{
      this.getMaterials();
      $btn.button('reset');
      $("#alertDelMat").modal("hide");
    },(error)=>{
      $btn.button('reset');
      this.toastr.error(error.message);
      console.log(error);
    })
  }



  onCheckboxChange(val) {
    let ids = this.selectedMaterial.units.indexOf(val);
    if(ids == -1){
      this.selectedMaterial.units.push(val);
    }else{
      this.selectedMaterial.units.splice(ids, 1);  
    }
  }

  addMaterial(materialForm){

    this.isSubTypesUnique = this.checkUnique(this.selectedMaterial.subTypes);

    if(materialForm.valid && this.selectedMaterial.units.length && this.isSubTypesUnique){  
      let $btn = $('#productBtn');
      $btn.button('loading');
      
      this.errSuccMaterial = {isError:false,isSuccess:false,succMsg:"",errMsg:""};      
      if(!this.selectedMaterial.id){
        this.materialApi.addMaterial(
          	this.selectedMaterial.name,
          	this.selectedMaterial.units,
            this.selectedMaterial.loading,
            this.selectedMaterial.unloading,
            this.selectedMaterial.gst,
            this.selectedMaterial.royalty,
          	this.selectedMaterial.subTypes
        ).subscribe((success)=>{
            materialForm._submitted = false;
            $btn.button('reset');
            $("#materialModal").modal("hide");
            this.toastr.success("Successfully added");
            this.getMaterials();
        },(error)=>{
            $btn.button('reset');
            this.errSuccMaterial = {isError:true,isSuccess:false,succMsg:"",errMsg:error.message};
        })        
      }else{
        this.materialApi.editMaterial(
            this.selectedMaterial.id,
            this.selectedMaterial.name,
            this.selectedMaterial.units,
            this.selectedMaterial.loading,
            this.selectedMaterial.unloading,
            this.selectedMaterial.gst,
            this.selectedMaterial.royalty,
            this.selectedMaterial.subTypes
        ).subscribe((success)=>{
            materialForm._submitted = false;
            $btn.button('reset');
            $("#materialModal").modal("hide");
            this.toastr.success("Successfully added");
            this.getMaterials();
        },(error)=>{
          $btn.button('reset');
          this.errSuccMaterial = {isError:true,isSuccess:false,succMsg:"",errMsg:error.message};
        })        
      }
    }
  }

  checkUnique(arr){
    return _.uniq(arr).length === arr.length;
  }



  trackByIndex(index: number, value: number) {
    return index;
  }

}

