import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductApi, LoopBackConfig } from './../../sdk';
import { ToastrService } from 'ngx-toastr'; 

declare const $: any;
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
 
  params:any = {};
  products:any = [];
  baseUrl:string = "";
  isLoading     : any = false;

  selectedProd: any = {};
  status: string = "";
  constructor(private toastr: ToastrService, private productApi:ProductApi, private route:ActivatedRoute) { 
    this.route.params.subscribe((params)=>{
      this.params = params;
    })

  }

  ngOnInit() {
    this.getOwnerAllProduct();
    this.baseUrl = LoopBackConfig.getPath();
  }

  getOwnerAllProduct(){
    this.isLoading = true;
    this.productApi.getOwnerProductByAdmin(this.params.ownerId).subscribe((success)=>{
      this.products = success.success.data;
      this.isLoading = false;
      
    },(error)=>{
      this.isLoading = false;
      this.toastr.error(error.message);
    })
  }


  delProdConfModal(product,status){
    this.selectedProd = product;
    this.status = status;
    $("#delPrdouct").modal("show");
  }

  actDeactProduct(actDeactForm?:any){
    let $btn = $('#delBtn');
    $btn.button('loading');
    this.productApi.actDeactProductByAdmin(this.selectedProd.id, this.status).subscribe((success)=>{
      this.getOwnerAllProduct();
      $btn.button('reset');
      $("#delPrdouct").modal("hide");
      if(this.status == "active")
        this.toastr.success("Successfully activated product");
      else
        this.toastr.success("Successfully deactivated product");

    },(error)=>{
      $btn.button('reset');
      this.toastr.error(error.message); 
    });
  }


}
