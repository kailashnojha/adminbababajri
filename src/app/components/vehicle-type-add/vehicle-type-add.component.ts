import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { VehicleTypeApi } from "./../../sdk";
import { MultipartService } from './../../services/multipart/multipart.service';
import { ToastrService } from 'ngx-toastr';

declare const $: any;


@Component({
  selector: 'app-vehicle-type-add',
  templateUrl: './vehicle-type-add.component.html',
  styleUrls: ['./vehicle-type-add.component.css']
})
export class VehicleTypeAddComponent implements OnInit {
    params:any = {};
    vehicleType : any = {
        "name": "",
        /*"image": "",
        "clickImage": "",
        "driveImage": "",
        "updatedAt": "",*/
        "loadValue": []
    }      

    files:any={}
    errSuccVehicleType: any = {isError:false,isSuccess:false,succMsg:"",errMsg:""};
  constructor(private toastr: ToastrService, private multipartServiceApi:MultipartService, private vehicleTypeApi:VehicleTypeApi, private route:ActivatedRoute) { 
  }

  ngOnInit() {
  	this.route.params.subscribe((params)=>{
  		this.params = params;
  		console.log(this.params)
  		this.addNewLoad();	
  		this.getVehicleType();
  	})	

  }

  addNewLoad(){
  	this.vehicleType.loadValue.push({ 
    	"tyres": 0, "loading": 0, "unloading": 0, "loadKM": 0, "unloadKM": 0,
      "driverSalary": 0, "driverDailyExp": 0, "helper": 0, "gst": 0,
      "royalty": 0, "tollTax": 0, "driverSaving": 0, "nightStay": 0, "daily": 0,
      "minCharge": 0, "unitTONmin": 9, "unitTONmax": 0, "unitCUFmin": 0,
      "unitCUFmax": 0, "unitBRICKmin": 0, "unitBRICKmax": 0, "unitBAGmin": 0,
      "unitBAGmax": 0, "unitLITERmin":0, "unitLITERmax":0
    })
  }

  addVehicleType(vehicleTypeForm?:any){

  	let $btn = $('#vehicleTypeBtn');
    $btn.button('loading');
    let data = {
    	name : this.vehicleType.name,
    	loadValue : this.vehicleType.loadValue
    };
    let params = {
    	vehicleTypeId:this.vehicleType.id
    }
    this.errSuccVehicleType = {isError:false,isSuccess:false,succMsg:"",errMsg:""};
  	if(this.vehicleType.id){
	  	this.multipartServiceApi.editVehicleTypeApi({params:params,data:data,files:{image:this.files}}).subscribe((success)=>{
	  		// addCategoryForm._submitted = false;
        $btn.button('reset');
        this.toastr.success("Successfully edited");
	  	},(error)=>{
	    	$btn.button('reset');
        this.errSuccVehicleType = {isError:true,isSuccess:false,succMsg:"",errMsg:error.message};
	  	})
  	}else{
  		this.multipartServiceApi.addVehicleTypeApi({params:params,data:data,files:{image:this.files}}).subscribe((success)=>{
			$btn.button('reset');
        this.toastr.success("Successfully added");
	  	},(error)=>{
	  		$btn.button('reset');
        this.errSuccVehicleType = {isError:true,isSuccess:false,succMsg:"",errMsg:error.message};
	  	})
  	}	
  	  	
  }


  getVehicleType(){
  	if(this.params.vehicleTypeId){

  		this.vehicleTypeApi.getTypeById(this.params.vehicleTypeId).subscribe((success)=>{
  			this.vehicleType = success.success.data;
  		},(error)=>{
  			console.log(error);
  		})	
  	}
  }

  trackByIndex(index: number, value: number) {
    return index;
  }

}
