import { Component, OnInit } from '@angular/core';
import { VehicleTypeApi, LoopBackConfig } from './../../sdk';



@Component({
  selector: 'app-vehicle-type-list',
  templateUrl: './vehicle-type-list.component.html',
  styleUrls: ['./vehicle-type-list.component.css']
})
export class VehicleTypeListComponent implements OnInit {
  
  vehicleTypes:any = [];
  baseUrl:string;	
  constructor(private vehicleTypeApi:VehicleTypeApi) {
  	this.baseUrl = LoopBackConfig.getPath();
  }

  ngOnInit() {
  	this.getAllVehicleType();
  }

  getAllVehicleType(){
  	this.vehicleTypeApi.getTypes().subscribe((success)=>{
  		
  		this.vehicleTypes = success.success.data;
  		console.log(this.vehicleTypes);
  	},(error)=>{
  		console.log(error)
  	})
  }


}
