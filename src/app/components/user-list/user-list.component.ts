import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PeopleApi, LoopBackConfig } from './../../sdk';
import { ToastrService } from 'ngx-toastr';
declare const $: any;

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  params        : any;	
  page          : any = 1;
  skip          : any = 0;
  limit         : any = 10;
  totalCount    : any = 0;
  searchStr     : any = "";
  type          : any = "owner";
  users         : any = [];
  isLoading     : any = false;
  approvalValue : string;
  selectedUser  : any = {};
  options:any = {
    statusList:["Pending","Rejected","Approved"],
    selectedStatus:{
      text:"Pending",
      id:"Pending"
    },
  }

  status:any = "";
  baseUrl:string = "";
  queryParams:any = {};
  constructor(private router:Router, private ref: ChangeDetectorRef,private toastr: ToastrService, private peopleApi:PeopleApi, private route: ActivatedRoute) { 
  	
    this.route.queryParamMap.subscribe((obj:any)=>{
      
      this.queryParams.status = obj.params.status || "Pending";
      this.queryParams.page = obj.params.page || 1;
      this.page = this.queryParams.page;
      this.totalCount = this.page * this.limit;
      
      this.options.selectedStatus = {
        text : this.queryParams.status,
        id   : this.queryParams.status
      }
      
      this.route.params.subscribe(params => {
          this.params = params;
          this.getUserType();
          this.getAllUsers();
      });


    });  


  }

  ngOnInit() {
    this.baseUrl = LoopBackConfig.getPath();
  	// [user, owner, driver]
  	
  }

  getUserType(){

  	if(this.params.type == "customers"){
  		this.type = "user";
  	}else if(this.params.type == "owners"){
  		this.type = "owner";
  	}else if(this.params.type == "agents"){
  		this.type = "agent";
  	}else if(this.params.type == "drivers"){
       this.type = "driver"; 
    }else{
       this.type = "emitra"; 
    }
  }

  getAllUsers(){
    this.getStatus();
    // this.ref.detectChanges();
    // console.log(this.type)
    if(this.params.type == "customers"){
      this.status = undefined;
    }
    this.isLoading = true;
    this.skip = (this.page-1) * this.limit;
  	this.peopleApi.getAllUsers(this.status,this.type,this.searchStr,this.skip,this.limit).subscribe((success)=>{
  		this.isLoading = false;
      this.users = success.success.data;	
      this.totalCount = success.success.totalCount;
      /*console.log(success.success)
      console.log(this.totalCount)*/
  	},(error)=>{
      this.isLoading = false;
      this.toastr.error(error.message);
  	})
  }

  getStatus(){
    if(this.options.selectedStatus.text == "Approved"){
      this.status = 'approved';
    }else if(this.options.selectedStatus.text == "Pending"){
      this.status = 'pending';
    }else if(this.options.selectedStatus.text == "Rejected"){
      this.status = 'rejected';
    }

    this.queryParams.page = this.page;
    this.queryParams.status = this.options.selectedStatus.text;
    this.router.navigate([],{queryParams:this.queryParams});

    /*if(this.options.searchStr && this.options.searchStr !=""){
      this.filter.where.fullName = {like:this.options.searchStr,options:"i"};
    }*/
  }  

  changePage(){
    setTimeout(()=>{
      /*this.queryParams.page = this.page;
      this.router.navigate([],{queryParams:this.queryParams});*/
      this.getAllUsers();
    },0)
  }  

  selected(data?:any){
    this.getAllUsers();
  }

  openApprovalModal(value,user){
    this.approvalValue = value;
    this.selectedUser = user;
    $("#approveModal").modal("show");
  }

  adminApproval(appForm?:any){
    let $btn = $('#approvalBtn');
    $btn.button('loading');
    this.peopleApi.approval(this.selectedUser.id,this.approvalValue).subscribe((success)=>{
      $btn.button('reset');
      this.toastr.success("Successfully "+this.approvalValue);
      $("#approveModal").modal("hide");
      this.getAllUsers();
    },(error)=>{
      $btn.button('reset');
      this.toastr.error(error.message);
    })
  }

  openModal(user){
    this.selectedUser = user;
    // console.log(this.selectedUser);
    $("#formModal").modal("show");
  }




}
