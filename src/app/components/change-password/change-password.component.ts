import { Component, OnInit } from '@angular/core';
import { PeopleApi } from './../../sdk';
declare var $:any;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  cpResError:any = {};	
  passObj:any = {};
  constructor(private peopleApi:PeopleApi) { }

  ngOnInit() {

  }

  changePassword(cpForm){
      this.cpResError.isError = false;
      this.cpResError.msg = "";
      this.cpResError.isSuccess = false;
      this.cpResError.successMsg = "";
      if(cpForm.valid){
          let btn = $("#cpBtn");  
          btn.button('loading');
          this.peopleApi.changePassword(this.passObj.oldPassword,this.passObj.newPassword).subscribe((success)=>{
            btn.button('reset');
            $('#cpModal').modal("show");
            this.cpResError.isSuccess = true;
            this.cpResError.successMsg = "Password change successfully";
            cpForm.resetForm();
          },(error)=>{
            btn.button('reset');
            this.cpResError.isError = true;
            this.cpResError.msg = error.message;
          })
      }    		
  }



}
