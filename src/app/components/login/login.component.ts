import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { PeopleApi, OtpApi ,LoopBackAuth} from './../../sdk/index';
declare const $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
}) 
export class LoginComponent implements OnInit {

  	user:any = { 
      realm : "admin",	
      mobile:"",
      password:"" 
    };

    actionError:any = {
      isError:false,
      msg:""
    }

    otpResError:any = {
      isError:false,
      msg:"" 
    }

    actionResError:any = {
      isError:false,
      msg:"" 
    }
    cpResError:any = {
      isError:false,
      msg:"" 
    }
    resetMobile:string="";
    otp:string;
    resetOtpPeopleId:string;
    passObj:any = {};
    constructor(private auth:LoopBackAuth,private router:Router,private peopleApi:PeopleApi, private otpApi:OtpApi) { 
      
    }

    ngOnInit() {
      let authObj = this.auth.getCurrentUserData();
      if(authObj && authObj.realm == "admin" && (this.auth.getAccessTokenId() != null || this.auth.getAccessTokenId()!='')){
        return this.router.navigate(['/material']);
      }

    	$.backstretch("assets/img/login-bg.jpg", { speed: 500 });
    }

    login(loginForm:any){
      let _self = this;
      if(loginForm.valid){
        this.actionError.isError = false;
        this.actionError.msg = "";
        let rememberMe = false;
        this.user.role = "admin";
        this.user.realm = "admin";
        this.peopleApi.login(this.user,'user',rememberMe).subscribe((success)=>{
          this.router.navigate(['/material']);
        },(error)=>{
          
          this.actionError.isError = true;
          this.actionError.msg = error.message; 
        })      
      }   
     
    }

    openForgetpass(){
      this.actionResError.isError = false;
      this.actionResError.msg = "";
      this.resetMobile = "";

      $('#forgetModal').modal({backdrop: 'static', keyboard: false});
    }

    resetPassSubmit(resForm,resOtpForm){
      this.actionResError.isError = false;
      this.actionResError.msg = "";
      if(resForm.valid){
        let btn = $("#resetBtn");  
        btn.button('loading');
        // $('#forgetModal').modal("hide");
        this.peopleApi.resetPassRequest(this.resetMobile).subscribe((success)=>{
          $('#forgetModal').modal("hide");
          $('#resetOtpModal').modal("show");
          resOtpForm.resetForm();
          this.resetOtpPeopleId = success.success.data.id;
          btn.button('reset');
        },(error)=>{
          btn.button('reset');
          this.actionResError.isError = true;
          this.actionResError.msg = error.message;
        })

      }
    }


    checkResetOtp(resetForm,cpForm){

      this.otpResError.isError = false;
      this.otpResError.msg = "";

      if(resetForm.valid){

        let btn = $("#resetOtpBtn");  
        btn.button('loading');

        this.otpApi.checkResetOtp(this.resetOtpPeopleId,this.otp).subscribe((success)=>{
          btn.button('reset');
          $('#resetOtpModal').modal("hide");
          $('#cpModal').modal("show");
          cpForm.resetForm();
        },(error)=>{
          btn.button('reset');
          this.otpResError.isError = true;
          this.otpResError.msg = error.message;
        })    
      }
  
    }


    changePassword(cpForm){
      this.cpResError.isError = false;
      this.cpResError.msg = "";
      this.cpResError.isSuccess = false;
      this.cpResError.successMsg = "";
      if(cpForm.valid){
        if(this.passObj.password == this.passObj.conPassword){
          let btn = $("#cpBtn");  
          btn.button('loading');
          this.peopleApi.resetPassword(this.resetOtpPeopleId,parseInt(this.otp),this.passObj.password).subscribe((success)=>{
            btn.button('reset');
            $('#cpModal').modal("show");
            this.cpResError.isSuccess = true;
            this.cpResError.successMsg = "Password reset successfully";
            cpForm.resetForm();
          },(error)=>{
            btn.button('reset');
            this.cpResError.isError = true;
            this.cpResError.msg = error.message;
          })
        }else{
          this.cpResError.isError = true;
          this.cpResError.msg = "Password and confirm password does not match"; 
        }
        
      }  
    }


}
