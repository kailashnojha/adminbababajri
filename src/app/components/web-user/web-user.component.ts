import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import { PeopleApi } from './../../sdk';
import { ToastrService } from 'ngx-toastr';
import { PeopleApi, WebUserApi } from './../../sdk';

declare const $: any;
@Component({
  selector: 'app-web-user',
  templateUrl: './web-user.component.html',
  styleUrls: ['./web-user.component.css']
})
export class WebUserComponent implements OnInit {
  params        : any;	
  page          : any = 1;
  skip          : any = 0;
  limit         : any = 10;
  totalCount    : any = 0;
  searchStr     : any = "";
  type          : any = "owner";
  users         : any = [];
  isLoading     : any = false;
  approvalValue : string;
  selectedUser  : any = {};
  options:any = {
    statusList:["Pending","Rejected","Approved"],
    selectedStatus:{
      text:"Pending",
      id:"Pending"
    },
  }

  status:any = "";  

  constructor(private webUserApi:WebUserApi, private toastr: ToastrService, private peopleApi:PeopleApi, private route: ActivatedRoute) { }

  ngOnInit() {
  	this.route.params.subscribe(params => {
        this.params = params;
        this.getUserType();
        this.getAllUsers();
    });
  }

  getUserType(){
  	if(this.params.type == "emitra"){
  		this.type = "emitra";
  	}else{
      this.type = "webuser";
    }
  }

  getAllUsers(){
    this.getStatus();
    this.isLoading = true;

    if(this.type == "webuser"){
       this.status = "approved"; 
    }

    this.skip = (this.page-1) * this.limit;
  	this.webUserApi.getAllUsers(this.status,this.type,this.searchStr,this.skip,this.limit).subscribe((success)=>{
      this.isLoading = false;
      this.users = success.success.data;
      this.totalCount	= success.success.count;
  	},(error)=>{
      this.isLoading = false;
      this.toastr.error(error.message);
  	})
  }

  getStatus(){
    // console.log(this.options);
    if(this.options.selectedStatus.text == "Approved"){
      this.status = 'approved';
    }else if(this.options.selectedStatus.text == "Pending"){
      this.status = 'pending';
    }else if(this.options.selectedStatus.text == "Rejected"){
      this.status = 'rejected';
    }
    /*if(this.options.searchStr && this.options.searchStr !=""){
      this.filter.where.fullName = {like:this.options.searchStr,options:"i"};
    }*/
  }  

  changePage(){
    setTimeout(()=>{
      this.getAllUsers();
    },0)
  }  

  selected(data?:any){
    // this.options.selectedStatus = data;
    // console.log("selected data => ",data);
    this.getAllUsers();
  }

  openApprovalModal(value,user){
    this.approvalValue = value;
    this.selectedUser = user;
    $("#approveModal").modal("show");
  }

  adminApproval(addApproval?:any){
    let $btn = $('#approvalBtn');
    $btn.button('loading');
    this.webUserApi.approval(this.selectedUser.id,this.approvalValue).subscribe((success)=>{
      $btn.button('reset');
      this.toastr.success("Successfully "+this.approvalValue);
      $("#approveModal").modal("hide");
      this.getAllUsers();
    },(error)=>{
      $btn.button('reset');
      this.toastr.error(error.message);
    })
  }

  openDeactivateModal(user){
    this.selectedUser = user;
    $("#deactivateModal").modal("show");
  }

  activeDeactivate(actDeactForm?:any){
    let $btn = $('#actDeactBtn');
    $btn.button('loading');
    this.webUserApi.activateDeactivate(this.selectedUser.id).subscribe((success)=>{
      $btn.button('reset');
      let msg = success.success.isDeactivate == false ? "Successfully deactivated" : "Successfully activated";
      this.toastr.success(msg);
      $("#deactivateModal").modal("hide");
      this.getAllUsers();
    },(error)=>{
      $btn.button('reset');
      this.toastr.error(error.message);
    })
  }

  openProfileModal(user){
    this.selectedUser = user;
    $("#formModal").modal("show");
  }




}
