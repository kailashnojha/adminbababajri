import { Component, OnInit, ViewChild } from '@angular/core';
import { MaterialApi, SubTypeApi, LoopBackAuth } from './../../sdk/index';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from "@angular/router";
import { MultipartService } from "./../../services/multipart/multipart.service";
import { Observable } from 'rxjs';

declare const $: any;

@Component({
  selector: 'app-material-types',
  templateUrl: './material-types.component.html',
  styleUrls: ['./material-types.component.css']
})
export class MaterialTypesComponent implements OnInit {

  material : any = {};
  selectedMatType : any = {
    name : {
      en:"",
      hi:""
    },
    subTypes : [] 
  };	
  params  : any;		
  errSuccMaterialType : any = {
    isError : false,
    isSuccess : false,
    succMsg : "",
    errMsg : ""
  }
  isLoad:boolean = false;
  subTypesArr:Array<any> = [];

  @ViewChild("materialTypeForm") matTypeForm; 
  constructor(private subTypeApi:SubTypeApi, private multipartApi:MultipartService, private toastr: ToastrService, private materialApi:MaterialApi, private router : Router, private route: ActivatedRoute) { 
  	this.route.params.subscribe( params => {
  		this.params = params;
  	});
  }

  ngOnInit() {
    this.getAllSubtype();
  	this.getMaterial();	
  }

  getMaterial(){
    this.isLoad = true;
    this.materialApi.getSingleMaterial(this.params.materialId).subscribe((success)=>{
      this.isLoad = false;
      this.material = success.success.data;
      // console.log(this.material)
    },(error)=>{
      this.isLoad = false;
      this.toastr.error(error.message);
    })
  }    

  openModal(materialType){
    $("#materialTypeImg").val("");
    this.errSuccMaterialType = {
      isError : false,
      isSuccess : false,
      succMsg : "",
      errMsg : ""
    }

    if(materialType){
      this.selectedMatType = JSON.parse(JSON.stringify(materialType))//Object.assign({}, materialType);
      if( typeof this.selectedMatType.name == 'string' ){
        this.selectedMatType.name = {
          en:"",
          hi:""
        }
      }
    }else{
      this.matTypeForm.resetForm();
      this.selectedMatType = {
        subTypes:[],
        name:{
          en:"",
          hi:""
        }
      };
    }
  	
    this.errSuccMaterialType = {
      isError:false,
      isSuccess:false,
      succMsg:"",
      errMsg:""
    }

  	$("#materialTypeModal").modal("show")
  }

  onCheckboxChange(val) {
    let ids = this.selectedMatType.subTypes.indexOf(val);
    if(ids == -1){
      this.selectedMatType.subTypes.push(val);
    }else{
      this.selectedMatType.subTypes.splice(ids, 1);  
    }
  }

  openDelModal(materialType){
    this.selectedMatType = Object.assign({}, materialType);
    $("#alertDelMatType").modal("show");
  }

  delMatTypeData(){
    let $btn = $('#delMatTypeBtn');
    $btn.button('loading');
    this.materialApi.delMatType(this.selectedMatType.id).subscribe((success)=>{
      this.getMaterial();
      $btn.button('reset');
      $("#alertDelMatType").modal("hide");
    },(error)=>{
      $btn.button('reset');
      this.toastr.error(error.message);
      $("#alertDelMatType").modal("hide");
    })
  }

  getAllSubtype(){
    this.subTypeApi.getSubTypesObj().subscribe((success)=>{
      let data = success.success.data;
      this.subTypesArr = [];
      for(let x in data){
        this.subTypesArr.push({
          en:x,
          hi:data[x]
        })
      }
    },(error)=>{
      this.toastr.error(error.message);
    })
  }

  addMaterialType(matTypeForm){

    let data = {
      name           : this.selectedMatType.name,
      subTypes       : this.selectedMatType.subTypes,
      materialId     : this.params.materialId,
      materialTypeId : this.selectedMatType.id  
    }

    let files = this.selectedMatType.file;
    
    if(matTypeForm.valid){
      let $btn = $('#matTypeBtn');
      $btn.button('loading');
      this.multipartApi.addMaterialTypeApi({data:data,files:{image:files}}).subscribe((success)=>{
        this.getMaterial();
        $("#materialTypeModal").modal("hide");
        $btn.button('reset');
      },(error)=>{
        this.errSuccMaterialType = {
          isError:true,
          isSuccess:false,
          succMsg:"",
          errMsg:error.message
        }
        $btn.button('reset');
      })
    }
  }

}
