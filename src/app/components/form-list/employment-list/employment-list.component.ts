import { Component, OnInit } from '@angular/core';
import { WebUserApi, RequestEmploymentApi, LoopBackConfig } from './../../../sdk';
import { ToastrService } from 'ngx-toastr';
import { MultipartService } from './../../../services/multipart/multipart.service';
declare const $: any;


@Component({
  selector: 'app-employment-list',
  templateUrl: './employment-list.component.html',
  styleUrls: ['./employment-list.component.css']
})
export class EmploymentListComponent implements OnInit {

  forms:Array<any> = [];
  selectedForm:any = {};
  isLoading:boolean = false;
  options:any = {
	 	page          : 1,
		skip          : 0,
		limit         : 10,
		totalCount    : 0,
    selectedUser : "All"
  }
  baseUrl:string = "";
  emitraList:any = [];

  constructor(private webUserApi:WebUserApi, private multipartApi:MultipartService ,private toastr: ToastrService, private requestEmploymentApi:RequestEmploymentApi) { }

  ngOnInit() {
  	this.baseUrl = LoopBackConfig.getPath();
    this.getAllEmitra();
  }

  getForms(){
    let skip = (this.options.page-1)*this.options.limit;
    let limit = this.options.limit; 
    let creatorId = undefined;
    if(this.options.selectedUser != "All"){
      creatorId = this.options.selectedUser;
    }
    this.isLoading = true;
    this.requestEmploymentApi.getAllForms(skip,limit,creatorId).subscribe((success)=>{
      this.forms = success.success.data;
      this.options.totalCount = success.success.count;
      this.isLoading = false;
    },(error)=>{
      this.isLoading = false;
      this.toastr.error(error.message);
    })
  }

  selected(data){
    setTimeout(()=>{
      this.getForms();
    },0)
    
  }

  getAllEmitra(){
   	this.webUserApi.getAllEmitra().subscribe((success)=>{
      this.emitraList = success.success.data;
      this.emitraList.unshift({
        firstName : "All",
        id   : "All"
      })
      // console.log(this.emitraList);
      this.getForms();
    },(error)=>{
      this.toastr.error(error.message);
  	})
  }


  changePage(){
    setTimeout(()=>{
      this.getForms();
    },0)
  }  

  openModal(form){
    this.selectedForm = form;
    console.log(this.selectedForm)
    $("#formModal").modal("show");
  }

  downloadFile(url,name){
    this.multipartApi.downloadFile(url,name);
  }



}
