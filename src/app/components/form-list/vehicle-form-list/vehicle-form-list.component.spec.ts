import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleFormListComponent } from './vehicle-form-list.component';

describe('VehicleFormListComponent', () => {
  let component: VehicleFormListComponent;
  let fixture: ComponentFixture<VehicleFormListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleFormListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFormListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
