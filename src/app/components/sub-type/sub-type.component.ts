import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SubTypeApi } from './../../sdk';

declare var $:any;


@Component({
  selector: 'app-sub-type',
  templateUrl: './sub-type.component.html',
  styleUrls: ['./sub-type.component.css']
})
export class SubTypeComponent implements OnInit {

	subTypes : Array<any> = [];
	options  : any = {};
	subType  : any = {
		name : {
			en : "",
			hi : ""
		}
	}
	isLoading:boolean = false;
	@ViewChild("subTypeForm") subTypeForm;
  	constructor(private toastr: ToastrService, private subTypeApi:SubTypeApi) { }

  	ngOnInit() {
  		this.getAllSubTypes();
  	}

  	getAllSubTypes(){
  		this.subTypeApi.getSubTypeByAdmin().subscribe((success)=>{
  			this.subTypes = success.success.data;
  			console.log(this.subTypes);
  		},(error)=>{	
  			this.toastr.error(error.message);
  			// console.log(error);
  		})
  	}

  	createOrUpdate(subTypeForm){
	    if(subTypeForm.valid){  
	      	let $btn = $('#submitBtn');
	      	$btn.button('loading');
	      	// this.errSuccMaterial = {isError:false,isSuccess:false,succMsg:"",errMsg:""};      
	        this.subTypeApi.addOrUpdate(
	          	this.subType.name,
	          	this.subType.id
	        ).subscribe((success)=>{
	            subTypeForm._submitted = false;
	            $btn.button('reset');
	            $("#subTypeModal").modal("hide");
	            if(this.subType.id)
	            	this.toastr.success("Successfully edited");
	            else
	            	this.toastr.success("Successfully added");
	            this.getAllSubTypes();
	        },(error)=>{
	            $btn.button('reset');
	            // this.errSuccMaterial = {isError:true,isSuccess:false,succMsg:"",errMsg:error.message};
	        })        
	    }
  	}


  	openModal(type?:any){

	    if(type) 
	        this.subType = JSON.parse(JSON.stringify(type))//Object.assign({}, type);
	    else{
	        this.subTypeForm.resetForm();
	        this.subType = {
				name : {
					en : "",
					hi : ""
				}
			};
	    }  
	    // console.log(this.material)
	    /*this.errSuccMaterial = {
	        isError:false,
	        isSuccess:false,
	        succMsg:"",
	        errMsg:""
	    }*/

	    console.log(this.subType);

      	$("#subTypeModal").modal("show")
  	}	



}
