import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderRequestApi, LoopBackConfig } from './../../sdk';
import { ToastrService } from 'ngx-toastr'; 

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  
	params:any = {};
  	orders:any = [];
  	baseUrl:string = "";  
  	modal_cmnt:any;
  	span_cmnt:any;
  	selectedOrder:any = {};
  	userData: any = {};
  	options = {
  		page    : 1,
  		limit   : 10,
  		totalCount: 0
  	}
  	isLoading     : any = false;
    constructor(private orderRequestApi:OrderRequestApi, private toastr: ToastrService, private route:ActivatedRoute) { 
    	this.route.params.subscribe((params)=>{
	      this.params = params;
	    })
	    this.baseUrl = LoopBackConfig.getPath();
    }

    ngOnInit() {
		// Get the modal
		this.modal_cmnt = document.getElementById('myModal_cmnt');
		// this.btn_cmnt = document.getElementById("myBtn_cmnt");
		this.span_cmnt = document.getElementsByClassName("close_cmnt")[0];
		/*btn_cmnt.onclick = function() {
		    modal_cmnt.style.display = "block";
		}
		span_cmnt.onclick = function() {
		    modal_cmnt.style.display = "none";	
		}
		window.onclick = function(event) {
		    if (event.target == modal_cmnt) {
		        modal_cmnt.style.display = "none";
		    }
		}*/

		this.getAllOrders();
    }

    onPriceClick(order){
    	this.selectedOrder = order;
    	this.modal_cmnt.style.display = "block";
    }

    onCloseBtnClick(){
    	this.modal_cmnt.style.display = "none";
    }

	myFunction(id) {
		var popup_cmnt = document.getElementById(id);
	    popup_cmnt.classList.toggle("show_cmnt");
	}

	getAllOrders(){

		let skip = (this.options.page-1) * this.options.limit;
		let limit = this.options.limit;
		this.isLoading = true;
		this.orderRequestApi.getAllOwnerOrderByAdmin(this.params.ownerId, skip, limit).subscribe((success)=>{
			this.orders = success.success.data;
			this.options.totalCount = success.success.count;
			this.userData = success.success.userData;
			this.isLoading = false;
		},(error)=>{
			this.isLoading = false;
			this.toastr.error(error.message);
		})
	}

	changePage(){
		setTimeout(()=>{
			this.getAllOrders();
		},0)
	}

}
