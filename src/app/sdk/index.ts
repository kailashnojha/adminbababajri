/* tslint:disable */
/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MIT 2016 Jonathan Casarrubias
* @version 2.1.0
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root 
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDK[Browser|Node|Native]Module } from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDK[Browser|Node|Native]Module.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/
import { ErrorHandler } from './services/core/error.service';
import { LoopBackAuth } from './services/core/auth.service';
import { LoggerService } from './services/custom/logger.service';
import { SDKModels } from './services/custom/SDKModels';
import { InternalStorage, SDKStorage } from './storage/storage.swaps';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CookieBrowser } from './storage/cookie.browser';
import { StorageBrowser } from './storage/storage.browser';
import { SocketBrowser } from './sockets/socket.browser';
import { SocketDriver } from './sockets/socket.driver';
import { SocketConnection } from './sockets/socket.connections';
import { RealTime } from './services/core/real.time';
import { PeopleApi } from './services/custom/People';
import { AccessTokenApi } from './services/custom/AccessToken';
import { EmailApi } from './services/custom/Email';
import { OtpApi } from './services/custom/Otp';
import { MaterialApi } from './services/custom/Material';
import { MaterialTypeApi } from './services/custom/MaterialType';
import { SubTypeApi } from './services/custom/SubType';
import { VehicleTypeApi } from './services/custom/VehicleType';
import { ContainerApi } from './services/custom/Container';
import { VehicleApi } from './services/custom/Vehicle';
import { RatingApi } from './services/custom/Rating';
import { MaterialRequestApi } from './services/custom/MaterialRequest';
import { StoreApi } from './services/custom/Store';
import { ProductApi } from './services/custom/Product';
import { OrderRequestApi } from './services/custom/OrderRequest';
import { WebUserApi } from './services/custom/WebUser';
import { InstantRequestApi } from './services/custom/InstantRequest';
import { NotificationApi } from './services/custom/Notification';
import { RequestDealershipApi } from './services/custom/RequestDealership';
import { RequestEmploymentApi } from './services/custom/RequestEmployment';
import { RequestVehicleApi } from './services/custom/RequestVehicle';
/**
* @module SDKBrowserModule
* @description
* This module should be imported when building a Web Application in the following scenarios:
*
*  1.- Regular web application
*  2.- Angular universal application (Browser Portion)
*  3.- Progressive applications (Angular Mobile, Ionic, WebViews, etc)
**/
@NgModule({
  imports:      [ CommonModule, HttpClientModule ],
  declarations: [ ],
  exports:      [ ],
  providers:    [
    ErrorHandler,
    SocketConnection
  ]
})
export class SDKBrowserModule {
  static forRoot(internalStorageProvider: any = {
    provide: InternalStorage,
    useClass: CookieBrowser
  }): ModuleWithProviders {
    return {
      ngModule  : SDKBrowserModule,
      providers : [
        LoopBackAuth,
        LoggerService,
        SDKModels,
        RealTime,
        PeopleApi,
        AccessTokenApi,
        EmailApi,
        OtpApi,
        MaterialApi,
        MaterialTypeApi,
        SubTypeApi,
        VehicleTypeApi,
        ContainerApi,
        VehicleApi,
        RatingApi,
        MaterialRequestApi,
        StoreApi,
        ProductApi,
        OrderRequestApi,
        WebUserApi,
        InstantRequestApi,
        NotificationApi,
        RequestDealershipApi,
        RequestEmploymentApi,
        RequestVehicleApi,
        internalStorageProvider,
        { provide: SDKStorage, useClass: StorageBrowser },
        { provide: SocketDriver, useClass: SocketBrowser }
      ]
    };
  }
}
/**
* Have Fun!!!
* - Jon
**/
export * from './models/index';
export * from './services/index';
export * from './lb.config';
export * from './storage/storage.swaps';
export { CookieBrowser } from './storage/cookie.browser';
export { StorageBrowser } from './storage/storage.browser';

