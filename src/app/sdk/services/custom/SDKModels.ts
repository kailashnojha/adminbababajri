/* tslint:disable */
import { Injectable } from '@angular/core';
import { People } from '../../models/People';
import { AccessToken } from '../../models/AccessToken';
import { Email } from '../../models/Email';
import { Otp } from '../../models/Otp';
import { Material } from '../../models/Material';
import { MaterialType } from '../../models/MaterialType';
import { VehicleType } from '../../models/VehicleType';
import { Container } from '../../models/Container';
import { Vehicle } from '../../models/Vehicle';
import { Rating } from '../../models/Rating';
import { MaterialRequest } from '../../models/MaterialRequest';
import { Store } from '../../models/Store';
import { Product } from '../../models/Product';
import { OrderRequest } from '../../models/OrderRequest';
import { InstantRequest } from '../../models/InstantRequest';
import { Notification } from '../../models/Notification';
import { SubType } from '../../models/SubType';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    People: People,
    AccessToken: AccessToken,
    Email: Email,
    Otp: Otp,
    Material: Material,
    MaterialType: MaterialType,
    VehicleType: VehicleType,
    Container: Container,
    Vehicle: Vehicle,
    Rating: Rating,
    MaterialRequest: MaterialRequest,
    Store: Store,
    Product: Product,
    OrderRequest: OrderRequest,
    InstantRequest: InstantRequest,
    Notification: Notification,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
