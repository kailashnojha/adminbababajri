/* tslint:disable */

declare var Object: any;
export interface RequestEmploymentInterface {
  "id"?: any;
}

export class RequestEmployment implements RequestEmploymentInterface {
  "id": any;
  constructor(data?: RequestEmploymentInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `RequestEmployment`.
   */
  public static getModelName() {
    return "RequestEmployment";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of RequestEmployment for dynamic purposes.
  **/
  public static factory(data: RequestEmploymentInterface): RequestEmployment{
    return new RequestEmployment(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'RequestEmployment',
      plural: 'RequestEmployments',
      path: 'RequestEmployments',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
