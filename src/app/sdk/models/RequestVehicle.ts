/* tslint:disable */

declare var Object: any;
export interface RequestVehicleInterface {
  "id"?: any;
}

export class RequestVehicle implements RequestVehicleInterface {
  "id": any;
  constructor(data?: RequestVehicleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `RequestVehicle`.
   */
  public static getModelName() {
    return "RequestVehicle";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of RequestVehicle for dynamic purposes.
  **/
  public static factory(data: RequestVehicleInterface): RequestVehicle{
    return new RequestVehicle(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'RequestVehicle',
      plural: 'RequestVehicles',
      path: 'RequestVehicles',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
