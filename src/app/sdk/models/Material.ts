/* tslint:disable */

declare var Object: any;
export interface MaterialInterface {
  "name"?: string;
  "createdAt"?: Date;
  "updatedAt"?: Date;
  "units"?: any;
  "id"?: any;
  "types"?: Array<any>;
  materialTypes?: any[];
}

export class Material implements MaterialInterface {
  "name": string;
  "createdAt": Date;
  "updatedAt": Date;
  "units": any;
  "id": any;
  "types": Array<any>;
  materialTypes: any[];
  constructor(data?: MaterialInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Material`.
   */
  public static getModelName() {
    return "Material";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Material for dynamic purposes.
  **/
  public static factory(data: MaterialInterface): Material{
    return new Material(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Material',
      plural: 'Materials',
      path: 'Materials',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date'
        },
        "updatedAt": {
          name: 'updatedAt',
          type: 'Date'
        },
        "units": {
          name: 'units',
          type: 'any',
          default: <any>null
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "types": {
          name: 'types',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        materialTypes: {
          name: 'materialTypes',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'types',
          keyTo: 'id'
        },
      }
    }
  }
}
