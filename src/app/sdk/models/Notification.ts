/* tslint:disable */

declare var Object: any;
export interface NotificationInterface {
  "id"?: any;
  "orderRequestId"?: any;
  "senderId"?: any;
  "receiverId"?: any;
  orderRequest?: any;
  sender?: any;
  receiver?: any;
}

export class Notification implements NotificationInterface {
  "id": any;
  "orderRequestId": any;
  "senderId": any;
  "receiverId": any;
  orderRequest: any;
  sender: any;
  receiver: any;
  constructor(data?: NotificationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Notification`.
   */
  public static getModelName() {
    return "Notification";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Notification for dynamic purposes.
  **/
  public static factory(data: NotificationInterface): Notification{
    return new Notification(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Notification',
      plural: 'Notifications',
      path: 'Notifications',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "orderRequestId": {
          name: 'orderRequestId',
          type: 'any'
        },
        "senderId": {
          name: 'senderId',
          type: 'any'
        },
        "receiverId": {
          name: 'receiverId',
          type: 'any'
        },
      },
      relations: {
        orderRequest: {
          name: 'orderRequest',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'orderRequestId',
          keyTo: 'id'
        },
        sender: {
          name: 'sender',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'senderId',
          keyTo: 'id'
        },
        receiver: {
          name: 'receiver',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'receiverId',
          keyTo: 'id'
        },
      }
    }
  }
}
