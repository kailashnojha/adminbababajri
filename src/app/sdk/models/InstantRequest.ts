/* tslint:disable */

declare var Object: any;
export interface InstantRequestInterface {
  "id"?: any;
  "driverId"?: any;
  driver?: any;
}

export class InstantRequest implements InstantRequestInterface {
  "id": any;
  "driverId": any;
  driver: any;
  constructor(data?: InstantRequestInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `InstantRequest`.
   */
  public static getModelName() {
    return "InstantRequest";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of InstantRequest for dynamic purposes.
  **/
  public static factory(data: InstantRequestInterface): InstantRequest{
    return new InstantRequest(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'InstantRequest',
      plural: 'InstantRequests',
      path: 'InstantRequests',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "driverId": {
          name: 'driverId',
          type: 'any'
        },
      },
      relations: {
        driver: {
          name: 'driver',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'driverId',
          keyTo: 'id'
        },
      }
    }
  }
}
