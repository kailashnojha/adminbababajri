/* tslint:disable */
import {
  GeoPoint
} from '../index';

declare var Object: any;
export interface ProductInterface {
  "image"?: string;
  "materialId": any;
  "materialTypeId": any;
  "location": GeoPoint;
  "name"?: string;
  "address": any;
  "unit": string;
  "price": number;
  "id"?: any;
  material?: any;
  materialType?: any;
  orderRequests?: any[];
}

export class Product implements ProductInterface {
  "image": string;
  "materialId": any;
  "materialTypeId": any;
  "location": GeoPoint;
  "name": string;
  "address": any;
  "unit": string;
  "price": number;
  "id": any;
  material: any;
  materialType: any;
  orderRequests: any[];
  constructor(data?: ProductInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Product`.
   */
  public static getModelName() {
    return "Product";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Product for dynamic purposes.
  **/
  public static factory(data: ProductInterface): Product{
    return new Product(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Product',
      plural: 'Products',
      path: 'Products',
      idName: 'id',
      properties: {
        "image": {
          name: 'image',
          type: 'string',
          default: ''
        },
        "materialId": {
          name: 'materialId',
          type: 'any'
        },
        "materialTypeId": {
          name: 'materialTypeId',
          type: 'any'
        },
        "location": {
          name: 'location',
          type: 'GeoPoint'
        },
        "name": {
          name: 'name',
          type: 'string',
          default: ''
        },
        "address": {
          name: 'address',
          type: 'any'
        },
        "unit": {
          name: 'unit',
          type: 'string'
        },
        "price": {
          name: 'price',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        material: {
          name: 'material',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialId',
          keyTo: 'id'
        },
        materialType: {
          name: 'materialType',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialTypeId',
          keyTo: 'id'
        },
        orderRequests: {
          name: 'orderRequests',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'productId'
        },
      }
    }
  }
}
