/* tslint:disable */

declare var Object: any;
export interface SubTypeInterface {
  "name": any;
  "createdAt"?: Date;
  "updatedAt"?: Date;
  "id"?: any;
}

export class SubType implements SubTypeInterface {
  "name": any;
  "createdAt": Date;
  "updatedAt": Date;
  "id": any;
  constructor(data?: SubTypeInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `SubType`.
   */
  public static getModelName() {
    return "SubType";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of SubType for dynamic purposes.
  **/
  public static factory(data: SubTypeInterface): SubType{
    return new SubType(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'SubType',
      plural: 'SubTypes',
      path: 'SubTypes',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'any'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date'
        },
        "updatedAt": {
          name: 'updatedAt',
          type: 'Date'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
