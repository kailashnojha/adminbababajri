/* tslint:disable */

declare var Object: any;
export interface VehicleTypeInterface {
  "name"?: string;
  "image"?: string;
  "id"?: any;
}

export class VehicleType implements VehicleTypeInterface {
  "name": string;
  "image": string;
  "id": any;
  constructor(data?: VehicleTypeInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `VehicleType`.
   */
  public static getModelName() {
    return "VehicleType";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of VehicleType for dynamic purposes.
  **/
  public static factory(data: VehicleTypeInterface): VehicleType{
    return new VehicleType(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'VehicleType',
      plural: 'VehicleTypes',
      path: 'VehicleTypes',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "image": {
          name: 'image',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
