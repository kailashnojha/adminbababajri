/* tslint:disable */

declare var Object: any;
export interface RequestDealershipInterface {
  "id"?: any;
  "creatorId"?: any;
  creator?: any;
}

export class RequestDealership implements RequestDealershipInterface {
  "id": any;
  "creatorId": any;
  creator: any;
  constructor(data?: RequestDealershipInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `RequestDealership`.
   */
  public static getModelName() {
    return "RequestDealership";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of RequestDealership for dynamic purposes.
  **/
  public static factory(data: RequestDealershipInterface): RequestDealership{
    return new RequestDealership(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'RequestDealership',
      plural: 'RequestDealerships',
      path: 'RequestDealerships',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "creatorId": {
          name: 'creatorId',
          type: 'any'
        },
      },
      relations: {
        creator: {
          name: 'creator',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'creatorId',
          keyTo: 'id'
        },
      }
    }
  }
}
