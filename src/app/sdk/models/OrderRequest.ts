/* tslint:disable */

declare var Object: any;
export interface OrderRequestInterface {
  "isRatingDone"?: boolean;
  "orderQuantity": string;
  "unit": string;
  "deliveryStatus"?: string;
  "id"?: any;
  "productId"?: any;
  "materialId"?: any;
  "materialTypeId"?: any;
  "vehicleId"?: any;
  "driverId"?: any;
  "customerId"?: any;
  "ownerId"?: any;
  "agentId"?: any;
  product?: any;
  material?: any;
  materialType?: any;
  vehicle?: any;
  driver?: any;
  customer?: any;
  owner?: any;
  agent?: any;
  rating?: any;
}

export class OrderRequest implements OrderRequestInterface {
  "isRatingDone": boolean;
  "orderQuantity": string;
  "unit": string;
  "deliveryStatus": string;
  "id": any;
  "productId": any;
  "materialId": any;
  "materialTypeId": any;
  "vehicleId": any;
  "driverId": any;
  "customerId": any;
  "ownerId": any;
  "agentId": any;
  product: any;
  material: any;
  materialType: any;
  vehicle: any;
  driver: any;
  customer: any;
  owner: any;
  agent: any;
  rating: any;
  constructor(data?: OrderRequestInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `OrderRequest`.
   */
  public static getModelName() {
    return "OrderRequest";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of OrderRequest for dynamic purposes.
  **/
  public static factory(data: OrderRequestInterface): OrderRequest{
    return new OrderRequest(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'OrderRequest',
      plural: 'OrderRequests',
      path: 'OrderRequests',
      idName: 'id',
      properties: {
        "isRatingDone": {
          name: 'isRatingDone',
          type: 'boolean',
          default: false
        },
        "orderQuantity": {
          name: 'orderQuantity',
          type: 'string'
        },
        "unit": {
          name: 'unit',
          type: 'string'
        },
        "deliveryStatus": {
          name: 'deliveryStatus',
          type: 'string',
          default: 'Pending'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "productId": {
          name: 'productId',
          type: 'any'
        },
        "materialId": {
          name: 'materialId',
          type: 'any'
        },
        "materialTypeId": {
          name: 'materialTypeId',
          type: 'any'
        },
        "vehicleId": {
          name: 'vehicleId',
          type: 'any'
        },
        "driverId": {
          name: 'driverId',
          type: 'any'
        },
        "customerId": {
          name: 'customerId',
          type: 'any'
        },
        "ownerId": {
          name: 'ownerId',
          type: 'any'
        },
        "agentId": {
          name: 'agentId',
          type: 'any'
        },
      },
      relations: {
        product: {
          name: 'product',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'productId',
          keyTo: 'id'
        },
        material: {
          name: 'material',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialId',
          keyTo: 'id'
        },
        materialType: {
          name: 'materialType',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialTypeId',
          keyTo: 'id'
        },
        vehicle: {
          name: 'vehicle',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'vehicleId',
          keyTo: 'id'
        },
        driver: {
          name: 'driver',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'driverId',
          keyTo: 'id'
        },
        customer: {
          name: 'customer',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'customerId',
          keyTo: 'id'
        },
        owner: {
          name: 'owner',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'ownerId',
          keyTo: 'id'
        },
        agent: {
          name: 'agent',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'agentId',
          keyTo: 'id'
        },
        rating: {
          name: 'rating',
          type: 'any',
          model: '',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'orderRequestId'
        },
      }
    }
  }
}
