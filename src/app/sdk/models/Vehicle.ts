/* tslint:disable */

declare var Object: any;
export interface VehicleInterface {
  "truckNumber": string;
  "registrationNo"?: string;
  "bodyType"?: string;
  "approveStatus"?: string;
  "id"?: any;
  "vehicleTypeId"?: any;
  "ownerId"?: any;
  "peopleId"?: any;
  vehicleType?: any;
  owner?: any;
}

export class Vehicle implements VehicleInterface {
  "truckNumber": string;
  "registrationNo": string;
  "bodyType": string;
  "approveStatus": string;
  "id": any;
  "vehicleTypeId": any;
  "ownerId": any;
  "peopleId": any;
  vehicleType: any;
  owner: any;
  constructor(data?: VehicleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Vehicle`.
   */
  public static getModelName() {
    return "Vehicle";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Vehicle for dynamic purposes.
  **/
  public static factory(data: VehicleInterface): Vehicle{
    return new Vehicle(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Vehicle',
      plural: 'Vehicles',
      path: 'Vehicles',
      idName: 'id',
      properties: {
        "truckNumber": {
          name: 'truckNumber',
          type: 'string'
        },
        "registrationNo": {
          name: 'registrationNo',
          type: 'string'
        },
        "bodyType": {
          name: 'bodyType',
          type: 'string'
        },
        "approveStatus": {
          name: 'approveStatus',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "vehicleTypeId": {
          name: 'vehicleTypeId',
          type: 'any'
        },
        "ownerId": {
          name: 'ownerId',
          type: 'any'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'any'
        },
      },
      relations: {
        vehicleType: {
          name: 'vehicleType',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'vehicleTypeId',
          keyTo: 'id'
        },
        owner: {
          name: 'owner',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'ownerId',
          keyTo: 'id'
        },
      }
    }
  }
}
