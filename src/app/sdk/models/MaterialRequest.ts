/* tslint:disable */

declare var Object: any;
export interface MaterialRequestInterface {
  "qty"?: number;
  "qtyUnit"?: string;
  "materialUnitPrice"?: number;
  "totalPrice"?: number;
  "id"?: any;
  "materialId"?: any;
  "materialTypeId"?: any;
  "providerId"?: any;
  "customerId"?: any;
  material?: any;
  materialType?: any;
  provider?: any;
  customer?: any;
}

export class MaterialRequest implements MaterialRequestInterface {
  "qty": number;
  "qtyUnit": string;
  "materialUnitPrice": number;
  "totalPrice": number;
  "id": any;
  "materialId": any;
  "materialTypeId": any;
  "providerId": any;
  "customerId": any;
  material: any;
  materialType: any;
  provider: any;
  customer: any;
  constructor(data?: MaterialRequestInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `MaterialRequest`.
   */
  public static getModelName() {
    return "MaterialRequest";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of MaterialRequest for dynamic purposes.
  **/
  public static factory(data: MaterialRequestInterface): MaterialRequest{
    return new MaterialRequest(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'MaterialRequest',
      plural: 'MaterialRequests',
      path: 'MaterialRequests',
      idName: 'id',
      properties: {
        "qty": {
          name: 'qty',
          type: 'number'
        },
        "qtyUnit": {
          name: 'qtyUnit',
          type: 'string'
        },
        "materialUnitPrice": {
          name: 'materialUnitPrice',
          type: 'number'
        },
        "totalPrice": {
          name: 'totalPrice',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "materialId": {
          name: 'materialId',
          type: 'any'
        },
        "materialTypeId": {
          name: 'materialTypeId',
          type: 'any'
        },
        "providerId": {
          name: 'providerId',
          type: 'any'
        },
        "customerId": {
          name: 'customerId',
          type: 'any'
        },
      },
      relations: {
        material: {
          name: 'material',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialId',
          keyTo: 'id'
        },
        materialType: {
          name: 'materialType',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialTypeId',
          keyTo: 'id'
        },
        provider: {
          name: 'provider',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'providerId',
          keyTo: 'id'
        },
        customer: {
          name: 'customer',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'customerId',
          keyTo: 'id'
        },
      }
    }
  }
}
