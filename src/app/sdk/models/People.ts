/* tslint:disable */

declare var Object: any;
export interface PeopleInterface {
  "billImg"?: string;
  "loadingVehicleImg"?: string;
  "rating"?: any;
  "adminApproval": string;
  "realm"?: string;
  "username"?: string;
  "email"?: string;
  "emailVerified"?: boolean;
  "id"?: any;
  "materialId"?: any;
  "materialTypeId"?: any;
  "orderRequestId"?: any;
  "password"?: string;
  accessTokens?: any[];
  material?: any;
  materialType?: any;
  vehicle?: any;
  orderRequest?: any;
  roleMappings?: any[];
  identities?: any[];
  credentials?: any[];
}

export class People implements PeopleInterface {
  "billImg": string;
  "loadingVehicleImg": string;
  "rating": any;
  "adminApproval": string;
  "realm": string;
  "username": string;
  "email": string;
  "emailVerified": boolean;
  "id": any;
  "materialId": any;
  "materialTypeId": any;
  "orderRequestId": any;
  "password": string;
  accessTokens: any[];
  material: any;
  materialType: any;
  vehicle: any;
  orderRequest: any;
  roleMappings: any[];
  identities: any[];
  credentials: any[];
  constructor(data?: PeopleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `People`.
   */
  public static getModelName() {
    return "People";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of People for dynamic purposes.
  **/
  public static factory(data: PeopleInterface): People{
    return new People(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'People',
      plural: 'People',
      path: 'People',
      idName: 'id',
      properties: {
        "billImg": {
          name: 'billImg',
          type: 'string',
          default: ''
        },
        "loadingVehicleImg": {
          name: 'loadingVehicleImg',
          type: 'string',
          default: ''
        },
        "rating": {
          name: 'rating',
          type: 'any',
          default: <any>null
        },
        "adminApproval": {
          name: 'adminApproval',
          type: 'string',
          default: 'pending'
        },
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "materialId": {
          name: 'materialId',
          type: 'any'
        },
        "materialTypeId": {
          name: 'materialTypeId',
          type: 'any'
        },
        "orderRequestId": {
          name: 'orderRequestId',
          type: 'any'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
        material: {
          name: 'material',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialId',
          keyTo: 'id'
        },
        materialType: {
          name: 'materialType',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialTypeId',
          keyTo: 'id'
        },
        vehicle: {
          name: 'vehicle',
          type: 'any',
          model: '',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
        orderRequest: {
          name: 'orderRequest',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'orderRequestId',
          keyTo: 'id'
        },
        roleMappings: {
          name: 'roleMappings',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'principalId'
        },
        identities: {
          name: 'identities',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
        credentials: {
          name: 'credentials',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
      }
    }
  }
}
