/* tslint:disable */

declare var Object: any;
export interface StoreInterface {
  "address"?: any;
  "id"?: any;
  "materialId"?: any;
  "materialTypeId"?: any;
  "ownerId"?: any;
  material?: any;
  materialType?: any;
  owner?: any;
}

export class Store implements StoreInterface {
  "address": any;
  "id": any;
  "materialId": any;
  "materialTypeId": any;
  "ownerId": any;
  material: any;
  materialType: any;
  owner: any;
  constructor(data?: StoreInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Store`.
   */
  public static getModelName() {
    return "Store";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Store for dynamic purposes.
  **/
  public static factory(data: StoreInterface): Store{
    return new Store(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Store',
      plural: 'Stores',
      path: 'Stores',
      idName: 'id',
      properties: {
        "address": {
          name: 'address',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "materialId": {
          name: 'materialId',
          type: 'any'
        },
        "materialTypeId": {
          name: 'materialTypeId',
          type: 'any'
        },
        "ownerId": {
          name: 'ownerId',
          type: 'any'
        },
      },
      relations: {
        material: {
          name: 'material',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialId',
          keyTo: 'id'
        },
        materialType: {
          name: 'materialType',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'materialTypeId',
          keyTo: 'id'
        },
        owner: {
          name: 'owner',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'ownerId',
          keyTo: 'id'
        },
      }
    }
  }
}
