/* tslint:disable */

declare var Object: any;
export interface WebUserInterface {
  "fullName"?: string;
  "email"?: string;
  "mobile"?: string;
  "shopAddress"?: any;
  "permanentAddress"?: any;
  "employmentDone"?: boolean;
  "vehicleDone"?: boolean;
  "dealershipDone"?: boolean;
  "realm"?: string;
  "username"?: string;
  "emailVerified"?: boolean;
  "id"?: any;
  "password"?: string;
  accessTokens?: any[];
}

export class WebUser implements WebUserInterface {
  "fullName": string;
  "email": string;
  "mobile": string;
  "shopAddress": any;
  "permanentAddress": any;
  "employmentDone": boolean;
  "vehicleDone": boolean;
  "dealershipDone": boolean;
  "realm": string;
  "username": string;
  "emailVerified": boolean;
  "id": any;
  "password": string;
  accessTokens: any[];
  constructor(data?: WebUserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `WebUser`.
   */
  public static getModelName() {
    return "WebUser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of WebUser for dynamic purposes.
  **/
  public static factory(data: WebUserInterface): WebUser{
    return new WebUser(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'WebUser',
      plural: 'WebUsers',
      path: 'WebUsers',
      idName: 'id',
      properties: {
        "fullName": {
          name: 'fullName',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "mobile": {
          name: 'mobile',
          type: 'string'
        },
        "shopAddress": {
          name: 'shopAddress',
          type: 'any'
        },
        "permanentAddress": {
          name: 'permanentAddress',
          type: 'any'
        },
        "employmentDone": {
          name: 'employmentDone',
          type: 'boolean',
          default: false
        },
        "vehicleDone": {
          name: 'vehicleDone',
          type: 'boolean',
          default: false
        },
        "dealershipDone": {
          name: 'dealershipDone',
          type: 'boolean',
          default: false
        },
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
      }
    }
  }
}
