/* tslint:disable */

declare var Object: any;
export interface RatingInterface {
  "id"?: any;
  "orderRequestId"?: any;
}

export class Rating implements RatingInterface {
  "id": any;
  "orderRequestId": any;
  constructor(data?: RatingInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Rating`.
   */
  public static getModelName() {
    return "Rating";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Rating for dynamic purposes.
  **/
  public static factory(data: RatingInterface): Rating{
    return new Rating(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Rating',
      plural: 'Ratings',
      path: 'Ratings',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "orderRequestId": {
          name: 'orderRequestId',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
