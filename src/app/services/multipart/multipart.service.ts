import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams, HttpResponse } from '@angular/common/http';
import { LoopBackAuth, LoopBackConfig }  from './../../sdk/index';
import { Observable } from 'rxjs';
import { catchError, map, filter } from 'rxjs/operators';
import { saveAs } from 'file-saver/FileSaver';
import { throwError } from 'rxjs';
@Injectable()
export class MultipartService {

  base:string;
  urls: any;

  constructor(private http:HttpClient,private auth:LoopBackAuth) { 
  	this.base = LoopBackConfig.getPath();
  	this.urls = {
      addVehicleType    : this.base+"/api/VehicleTypes/addType",
      editVehicleType    : this.base+"/api/VehicleTypes/editType",
      addMaterialType    : this.base+"/api/Materials/addMaterialType",
  	}
  }

  request(url,data){
    return this.http.post(url,this.getFormObj(data),this.getOption(data.params)).pipe(map((res: HttpResponse<{}>) => res.body),catchError((errorResponse) => {return throwError(errorResponse.error.error || 'Server error');}));
  }

  getFormObj(obj){
  	let fd = new FormData();
    if(obj.data && typeof obj.data == 'object'){

  		if(obj.data.extra){
  			delete obj.data.extra;
  		}

  		if(obj.data.address){
  			delete obj.data.address;
  		}

  		fd.append('data',JSON.stringify(obj.data));
    }

  	for(let x in obj.files){

  		for(let y in obj.files[x]){
  			fd.append(x,obj.files[x][y]);   
  		}

    }

  	return fd;
  }

  getOption(paramsData){
    paramsData = paramsData || {}; 
    let headers  = new HttpHeaders();
    
    for(let x in paramsData){
      if(paramsData[x] == undefined){
        delete paramsData[x];
      }
    }

    let params = Object.getOwnPropertyNames(paramsData)
                 .reduce((p, key) => p.set(key, paramsData[key]), new HttpParams());

    if (this.auth.getAccessTokenId()) {
      headers = headers.append(
        'Authorization',
        LoopBackConfig.getAuthPrefix() + this.auth.getAccessTokenId()
      );
    }
    return {headers:headers,params:params};
  }


  addVehicleTypeApi(data){
    return this.request(this.urls.addVehicleType,data); 
  }

  editVehicleTypeApi(data){
    return this.request(this.urls.editVehicleType,data); 
  }

  addMaterialTypeApi(data){
    return this.request(this.urls.addMaterialType,data); 
  }

// Extra File donwload service

  downloadFile(url,name){
    this.http.get(url,{responseType: "blob"}).subscribe((response:any)=>{
      saveAs(response, name);
    },(error)=>{
      
    })    
  }



}
