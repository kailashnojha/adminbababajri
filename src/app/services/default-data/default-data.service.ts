import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DefaultDataService {

  constructor() { }

  getUnits(){
  	return ["CUF","TON","BRICKS","BAG","LITER"];
  }

}
