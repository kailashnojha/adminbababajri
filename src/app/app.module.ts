// ************************** Modules *****************************************************
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingModule } from './routing/routing.module';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SDKBrowserModule } from './sdk/index';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// import {SelectModule} from 'ng2-select';
import { NgSelectModule } from '@ng-select/ng-select';

// **************************** Components **************************************************
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MaterialComponent } from './components/material/material.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserProfileComponent } from './components/user-list/user-profile/user-profile.component';
import { MaterialTypesComponent } from './components/material-types/material-types.component';
import { VehicleListComponent } from './components/vehicle-list/vehicle-list.component';
import { VehicleTypeListComponent } from './components/vehicle-type-list/vehicle-type-list.component';
import { VehicleTypeAddComponent } from './components/vehicle-type-add/vehicle-type-add.component';
import { WebUserComponent } from './components/web-user/web-user.component';
import { WebUserProfileComponent } from './components/web-user/web-user-profile/web-user-profile.component';
import { EmploymentListComponent } from './components/form-list/employment-list/employment-list.component';
import { DealershipListComponent } from './components/form-list/dealership-list/dealership-list.component';
import { VehicleFormListComponent } from './components/form-list/vehicle-form-list/vehicle-form-list.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { SubTypeComponent } from './components/sub-type/sub-type.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { OrderListComponent } from './components/order-list/order-list.component';


// *************************** Services ******************************************************
import { MultipartService } from './services/multipart/multipart.service';
import { ImageHeightWidthService } from './services/image-height-width/image-height-width.service';
import { DefaultDataService } from './services/default-data/default-data.service';

// *************************** Directives ******************************************************
import { FileDirective } from './directives/file/file.directive';



@NgModule({
  declarations: [
    FileDirective,
    AppComponent,
    LoginComponent,
    MaterialComponent,
    UserListComponent,
    UserProfileComponent,
    MaterialTypesComponent,
    VehicleListComponent,
    VehicleTypeListComponent,
    VehicleTypeAddComponent,
    WebUserComponent,
    WebUserProfileComponent,
    EmploymentListComponent,
    DealershipListComponent,
    VehicleFormListComponent,
    ChangePasswordComponent,
    SubTypeComponent,
    ProductListComponent,
    OrderListComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    // BootstrapAutocompleteModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot(), // ToastrModule added
    // SelectModule,
    SDKBrowserModule.forRoot(),
    NgbModule,
    // SelectModule,
    NgSelectModule
  ],
  providers: [
    MultipartService,
    ImageHeightWidthService,
    DefaultDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
