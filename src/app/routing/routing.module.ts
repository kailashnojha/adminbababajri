import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './../components/login/login.component';
import { UserListComponent } from './../components/user-list/user-list.component';

import { MaterialComponent } from './../components/material/material.component';
import { MaterialTypesComponent } from './../components/material-types/material-types.component';
import { SubTypeComponent } from './../components/sub-type/sub-type.component';

import { VehicleListComponent } from './../components/vehicle-list/vehicle-list.component';
import { VehicleTypeListComponent } from './../components/vehicle-type-list/vehicle-type-list.component';
import { VehicleTypeAddComponent } from './../components/vehicle-type-add/vehicle-type-add.component';
import { WebUserComponent } from './../components/web-user/web-user.component';
import { ChangePasswordComponent } from './../components/change-password/change-password.component';

import { EmploymentListComponent } from './../components/form-list/employment-list/employment-list.component';
import { DealershipListComponent } from './../components/form-list/dealership-list/dealership-list.component';
import { VehicleFormListComponent } from './../components/form-list/vehicle-form-list/vehicle-form-list.component';

import { ProductListComponent } from './../components/product-list/product-list.component';
import { OrderListComponent } from './../components/order-list/order-list.component';

const routes: Routes = [
	{ path: '', redirectTo: '/', pathMatch: 'full' },
	{ path: '', component: LoginComponent },

	{ path: 'material', component: MaterialComponent },
  { path: 'material-types/:materialId', component: MaterialTypesComponent },  
  { path: 'sub-types', component: SubTypeComponent },  


	{ path: 'user-list/:type', component: UserListComponent },
  { path: 'web-user/:type', component: WebUserComponent },        

  { path: 'vehicle-list', component: VehicleListComponent },
  { path: 'vehicle-type', component: VehicleTypeListComponent },    
  { path: 'vehicle-type-add', component: VehicleTypeAddComponent },    
  { path: 'vehicle-type-view/:vehicleTypeId', component: VehicleTypeAddComponent },    
  { path: 'vehicle-type-edit/:vehicleTypeId', component: VehicleTypeAddComponent },    

  { path: 'change-password', component: ChangePasswordComponent },    

  { path: 'employment-form-list', component: EmploymentListComponent },    
  { path: 'dealership-form-list', component: DealershipListComponent },
  { path: 'vehicle-form-list', component: VehicleFormListComponent },    

  { path: 'product-list/:ownerId', component: ProductListComponent },
  { path: 'order-list/:ownerId', component: OrderListComponent }      


]; 

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class RoutingModule { }
