import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router'
import { LoopBackConfig, LoopBackAuth, PeopleApi } from './sdk/index';
import { ToastrService } from 'ngx-toastr';
declare const $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{

 	title = 'app';
	isLoginPage:boolean= true;
	activeUrl:string="";
    constructor(private toastr: ToastrService, private peopleApi:PeopleApi, private router:Router){
    	// LoopBackConfig.setBaseURL('http://localhost:3010');
		// LoopBackConfig.setBaseURL('http://139.59.71.150:3010');
		LoopBackConfig.setBaseURL('https://api.babbabazri.com');
		LoopBackConfig.setApiVersion('api'); 
    }

	ngOnInit() {
		this.router.events.subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) {
				return;
			}
			// console.log(evt.url)
			this.activeUrl = evt.url;
			if(evt.url == "/" || evt.url.startsWith("/reset-password"))
				this.isLoginPage = true;
			else{
				$(".backstretch").remove();
				this.isLoginPage = false
			}
		})	
		this.checkNavBar();
	}	

	checkNavBar(){
		var wSize = $(window).width();
        if (wSize <= 768) {
            $('#container').addClass('sidebar-close');
            $('#sidebar > ul').hide();
        }

        if (wSize > 768) {
            $('#container').removeClass('sidebar-close');
            $('#sidebar > ul').show();
        }
	}

	navBar(){
		if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
	}
	
	navigate(url:string,params?:any){
		this.router.navigate([url]);
		this.checkNavBar()
	}

	logout(){
		this.peopleApi.logout().subscribe((success)=>{
			this.router.navigate(['/']);
		},(error)=>{
			if(error.statusCode == 401){
				this.router.navigate(['/']);
			}
		})
	}
}
